import { PartialType } from '@nestjs/mapped-types';
import { CreateTaskDto } from './create-task.dto';
import { IsOptional, IsString, MinLength } from 'class-validator';

export class UpdateTaskDto extends PartialType(CreateTaskDto) {

    @IsOptional()
    id!: number;

    @IsString()
    @MinLength(1)
    @IsOptional()
    name?: string;

    @IsOptional()
    description!: string;
}
