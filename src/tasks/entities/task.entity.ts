import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comment } from 'src/comments/entities/comment.entity';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column()
  description!: string;

  // Relación uno a muchos con Comment
  @OneToMany(() => Comment, comment => comment.task)
  comments!: Comment[];
  dueDate: any;
}
