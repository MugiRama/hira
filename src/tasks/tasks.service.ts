import { Injectable } from '@nestjs/common';
import { NotificationService } from '../notification/notification.service'; // Ajusta la ruta según tu estructura
import { Task } from './entities/task.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private readonly taskRepository: Repository<Task>,
    private readonly notificationService: NotificationService,
  ) {}
  async create(createTaskDto: CreateTaskDto) {
    const newTask = this.taskRepository.create(createTaskDto);
    const createdTask = await this.taskRepository.save(newTask);

    // Agregar lógica para enviar notificación si hay una fecha de vencimiento
    if (createTaskDto.dueDate) {
      this.notificationService.sendTaskExpirationNotification(createdTask.id);
    }

    return createdTask;
  }

  async update(id: number, updateTaskDto: UpdateTaskDto) {
    await this.taskRepository.update(id, updateTaskDto);
  
    // Agregar lógica para enviar notificación si hay una nueva fecha de vencimiento
    const updatedTask = await this.taskRepository.findOneBy({id});
  
    if (updatedTask && updatedTask.dueDate) {
      this.notificationService.sendTaskExpirationNotification(id);
    }
  
    return `Task con el ID ${id} ha sido modificado`;
  }

  async remove(id: number) {
    await this.taskRepository.delete(id);
    return `Task con el ID ${id} ha sido removido`;
  }
  async findOne(id: number) {
    return await this.taskRepository.findOne({ 
      where: { id },
      relations: ['comments']
    });
  }

  
  async findAll() {
    return await this.taskRepository.find();
  }

}
