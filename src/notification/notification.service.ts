// notificacion/notificacion.service.ts
import { Injectable } from '@nestjs/common';

@Injectable()
export class NotificationService {
  sendTaskExpirationNotification(taskId: number) {
    console.log(`Notificación: La tarea con ID ${taskId} ha vencido`);
  }
}
