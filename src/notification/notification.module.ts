// notification/notification.module.ts
import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { NotificationController } from './notification.controller';

@Module({
  controllers: [NotificationController],
  providers: [NotificationService],
  exports: [NotificationService], // Añade esto para que NotificationService esté disponible fuera de NotificationModule
})
export class NotificationModule {}
