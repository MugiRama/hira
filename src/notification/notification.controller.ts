import { Controller, Post,Param } from '@nestjs/common';
import { NotificationService } from './notification.service';


@Controller('notification')
export class NotificationController {
  constructor(private readonly notificationService: NotificationService) {}

  @Post(':taskId')
  sendNotification(@Param('taskId') taskId: string) {
    this.notificationService.sendTaskExpirationNotification(+taskId);
    return { message: 'Notificación enviada exitosamente' };
  }
}