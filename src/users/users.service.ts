// user.service.ts
import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const newUser = this.userRepository.create(createUserDto);
    return await this.userRepository.save(newUser);
  }

  async findOneByEmail(email: string) {
    return await this.userRepository.findOneBy({ email });
  }

  async findAll() {
    return await this.userRepository.find();
  }

  async findOne(id: number) {
    return await this.userRepository.findOneBy({ id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.userRepository.update(id, updateUserDto);
    return `El usuario con el ID ${id} ha sido modificado`;
  }

  async remove(id: number) {
    await this.userRepository.delete(id);
    return `El usuario con el ID ${id} ha sido removido`;
  }

  async assignRoles(userId: number, roles: string[]) {
    const user = await this.userRepository.findOneBy({ id: userId });
  
    if (user) {
      user.roles = roles;
      await this.userRepository.save(user);
    }
  }
  
  
}

