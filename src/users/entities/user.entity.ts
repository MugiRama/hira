import { Column, DeleteDateColumn, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable } from "typeorm";
import { Team } from "src/teams/entities/team.entity";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number | undefined;

    @Column()
    name: string | undefined;

    @Column({ unique: true, nullable: false })
    email: string | undefined;

    @Column({ nullable: false })
    password: string | undefined;

    @DeleteDateColumn()
    deletedAt: Date | undefined;

    @Column({ default: 'user' })
    role: string | undefined;

    @ManyToMany(() => Team, team => team.members)
    @JoinTable()
    teams: Team[] | undefined;

    @Column('simple-array', { nullable: true })
    roles: string[] | undefined; // Agrega esta línea para la propiedad 'roles'
}
