
import { IsOptional, IsString, MinLength } from 'class-validator';

export class UpdateUserDto  {

    @IsOptional()
    email?: string;

    @IsOptional()
    password?: string;

    @IsString()
    @MinLength(1)
    @IsOptional()
    name?: string;


 
}
