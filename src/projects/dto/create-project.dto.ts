import { IsString } from "class-validator";

export class CreateProjectDto {
    id!: number;
    @IsString()
    name?: string;
}
 