import { Column, DeleteDateColumn, Entity } from "typeorm";

@Entity()
export class Project {

    @Column({ primary: true, generated: true })
    id!: number;

    @Column()
    name!: string;
 
    @DeleteDateColumn()
    deletedAt!: Date;   

}
