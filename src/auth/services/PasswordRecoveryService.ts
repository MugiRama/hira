import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcryptjs from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class PasswordRecoveryService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) {}

    /**
     * Solicita el restablecimiento de la contraseña y envía un correo electrónico con el enlace de restablecimiento.
     * @param email Correo electrónico del usuario.
     * @returns Mensaje de éxito.
     */
    async requestPasswordReset(email: string): Promise<string> {
        const user = await this.usersService.findOneByEmail(email);
    
        if (!user) {
            throw new NotFoundException('User not found');
        }
    
        if (!user.email) {
            throw new BadRequestException('User email is missing');
        }
    
        // Generar un token de restablecimiento de contraseña y almacenarlo en la base de datos o en caché
        const resetToken = await this.generateResetToken(user.email);
    
        // Enviar el correo electrónico con el enlace de restablecimiento de contraseña que contiene el token
        // Implementa tu lógica para enviar correos electrónicos aquí
        // ...
    
        return `Password reset email sent successfully. Token: ${resetToken}`;
    }
    
    
    /**
     * Restablece la contraseña del usuario utilizando el token proporcionado.
     * @param email Correo electrónico del usuario.
     * @param newPassword Nueva contraseña.
     * @param resetToken Token de restablecimiento de contraseña.
     * @returns Mensaje de éxito.
     */
    async resetPassword(email: string, newPassword: string, resetToken: string): Promise<string> {
        const user = await this.usersService.findOneByEmail(email);
    
        if (!user) {
            throw new NotFoundException('User not found');
        }
    
        if (!resetToken) {
            throw new BadRequestException('Reset token is missing');
        }
    
        if (!user.email) {
            throw new BadRequestException('User email is missing');
        }
    
        await this.validateResetToken(user.email, resetToken);
    
        if (!user.id) {
            throw new BadRequestException('User ID is missing');
        }
    
        user.password = await bcryptjs.hash(newPassword, 10);
        await this.usersService.update(user.id, { password: user.password });
    
        return 'Password reset successfully';
    }
    


    /**
     * Genera un token único para el restablecimiento de contraseña.
     * @param email Correo electrónico del usuario.
     * @returns Token de restablecimiento de contraseña.
     */
    private async generateResetToken(email: string): Promise<string> {
        // Puedes personalizar la generación del token según tus necesidades
        // En este ejemplo, usaremos el servicio Jwt para generar un token único
        const payload = { email };
        return this.jwtService.signAsync(payload, { expiresIn: '1h' });
    }

    /**
     * Valida la autenticidad del token de restablecimiento de contraseña.
     * @param email Correo electrónico del usuario.
     * @param resetToken Token de restablecimiento de contraseña.
     */
    private async validateResetToken(_email: string, resetToken: string): Promise<void> {
        try {
            await this.jwtService.verifyAsync(resetToken);
    
            // Puedes realizar otras comprobaciones lógicas aquí según tus requisitos
    
        } catch (error) {
            throw new BadRequestException('Invalid or expired reset token');
        }
    }
    
    
    
}
