// auth.service.ts
import { BadRequestException, Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { RegisterDto } from '../dto/register.dto';
import * as bcryptjs from 'bcryptjs';
import { LoginDto } from '../dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { PasswordRecoveryService } from './PasswordRecoveryService';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        private readonly passwordRecoveryService: PasswordRecoveryService,
    ) {}

    async register({ name, email, password }: RegisterDto) {
        const user = await this.usersService.findOneByEmail(email);

        if (user) {
            throw new BadRequestException('User already exists');
        }

        return await this.usersService.create({
            name,
            email,
            password: await bcryptjs.hash(password, 10),
        });
    }

    async login({ email, password }: LoginDto) {
        const user = await this.usersService.findOneByEmail(email);
        if (!user) {
            throw new UnauthorizedException('Email is wrong');
        }

        if (!user.password) {
            throw new UnauthorizedException('Password is missing');
        }

        const isPasswordValid = await bcryptjs.compare(password, user.password);
        if (!isPasswordValid) {
            throw new UnauthorizedException('Password is wrong');
        }

        const payload = { email: user.email };

        const token = await this.jwtService.signAsync(payload);

        return {
            token,
            email,
        };
    }

    async requestPasswordReset(email: string) {
        return this.passwordRecoveryService.requestPasswordReset(email);
    }

    async resetPassword(email: string, newPassword: string, resetToken: string) {
        return this.passwordRecoveryService.resetPassword(email, newPassword, resetToken);
    }
}
