import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './services/auth.service';
import { PasswordRecoveryService } from './services/PasswordRecoveryService'; // Asegúrate de ajustar la ruta según tu estructura de archivos
import { UsersModule } from 'src/users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './constants/jwt.constants';

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: {expiresIn: '1d'},
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, PasswordRecoveryService], // Asegúrate de que PasswordRecoveryService esté importado aquí
})
export class AuthModule {}
