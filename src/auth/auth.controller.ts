// auth.controller.ts

import { Body, Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { AuthService } from './services/auth.service';
import { RegisterDto } from './dto/register.dto';
import { LoginDto } from './dto/login.dto';
import { AuthGuard } from './guard/auth.guard';
import { User } from 'src/users/entities/user.entity'; // Asegúrate de importar el tipo de usuario correcto

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService ){}

    @Post('register')
    register(
        @Body() registerDto: RegisterDto
    ) {
        return this.authService.register(registerDto);
    }

    @Post('login')
    login(
        @Body() loginDto: LoginDto,
    ) {
        console.log('LOGIN');
        return this.authService.login(loginDto);
    }

    @Get('profile')
    @UseGuards(AuthGuard)
    profile(
        @Request() req: { user: User } // Especifica el tipo de usuario
    ) {
        return req.user;
    }

    @Post('reset-password-request')
    async resetPasswordRequest(@Body() { email }: { email: string }) {
        return this.authService.requestPasswordReset(email);
    }

    @Post('reset-password')
    async resetPassword(
        @Body() { email, newPassword, resetToken }: { email: string, newPassword: string, resetToken: string },
    ) {
        return this.authService.resetPassword(email, newPassword, resetToken);
    }
}
