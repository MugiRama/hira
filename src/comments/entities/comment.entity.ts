import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Task } from 'src/tasks/entities/task.entity';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  text!: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt!: Date;

  @Column({ nullable: true })
  author!: string;

  // Relación muchos a uno con Task
  @ManyToOne(() => Task, task => task.comments, { nullable: false })
  task!: Task;
}
