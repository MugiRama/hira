// comments.controller.ts

import { Controller, Get, Post, Body, Patch, Param, Delete} from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';


@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  @Post(':taskId') // Modificado para incluir taskId en la ruta
  create(@Body() createCommentDto: CreateCommentDto, @Param('taskId') taskId: string) {
    return this.commentsService.create(createCommentDto, +taskId);
  }

  @Get(':taskId') // Modificado para incluir taskId en la ruta
  findAll(@Param('taskId') taskId: string) {
    return this.commentsService.findAll(+taskId);
  }

  @Get(':taskId/:id') // Modificado para incluir taskId en la ruta
  findOne(@Param('id') id: string) {
    return this.commentsService.findOne(+id);
  }

  @Patch(':taskId/:id') // Modificado para incluir taskId en la ruta
  update(@Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto) {
    return this.commentsService.update(+id, updateCommentDto);
  }

  @Delete(':taskId/:id') // Modificado para incluir taskId en la ruta
  remove(@Param('id') id: string) {
    return this.commentsService.remove(+id);
  }
}
 