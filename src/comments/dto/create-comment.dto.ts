

import { IsString, IsNotEmpty, IsNumber, IsDate } from 'class-validator';

export class CreateCommentDto {
  @IsString()
  @IsNotEmpty()
  text!: string;

  @IsNumber()
  taskId!: number; // Identificador de la tarea a la que pertenece el comentario

  
  @IsString()
  author!: string;

  @IsDate()
  createdAt!: Date;

 
}
