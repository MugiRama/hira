// update-comment.dto.ts

import { PartialType } from '@nestjs/mapped-types';
import { CreateCommentDto } from './create-comment.dto';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';

export class UpdateCommentDto extends PartialType(CreateCommentDto) {

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  text?: string;


}
