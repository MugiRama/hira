// comments.module.ts

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Comment } from './entities/comment.entity'; // Asegúrate de importar tu entidad Comment
import { CommentsService } from './comments.service';
import { CommentsController } from './comments.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Comment])], // Asegúrate de importar tu entidad Comment
  controllers: [CommentsController],
  providers: [CommentsService],
})
export class CommentsModule {}
