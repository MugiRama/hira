// comments.service.ts

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';
import { Comment } from './entities/comment.entity';
import { UpdateCommentDto } from './dto/update-comment.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
  ) {}

  async create(createCommentDto: CreateCommentDto, taskId: number) {
    const taskComment = this.commentRepository.create({
      ...createCommentDto,
      task: { id: taskId }, // Asocia el comentario a la tarea con el ID taskId
    });
  
    return await this.commentRepository.save(taskComment);
  }

  async findAll(taskId: number) {
    return await this.commentRepository.find({ where: { task: { id: taskId } } });
  }

  async findOne(id: number) {
    const comment = await this.commentRepository.findOneBy({id});
    if (!comment) {
      throw new NotFoundException(`Comentario con el ID ${id} no encontrado`);
    }
    return comment;
  }

  async update(id: number, updateCommentDto: UpdateCommentDto) {
    await this.commentRepository.update(id, updateCommentDto);
    return `Comentario con el ID ${id} ha sido modificado`;
  }

  async remove(id: number) {
    const comment = await this.commentRepository.findOneBy({id});
    if (!comment) {
      throw new NotFoundException(`Comentario con el ID ${id} no encontrado`);
    }

    await this.commentRepository.delete(id);
    return `Comentario con el ID ${id} ha sido eliminado`;
  }
}
