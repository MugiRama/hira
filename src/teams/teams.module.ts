import { Module } from '@nestjs/common';
import { TeamsService } from './teams.service';
import { TeamsController } from './teams.controller';
import { TypeOrmModule } from '@nestjs/typeorm'; // Asegúrate de importar TypeOrmModule
import { Team } from './entities/team.entity'; // Asegúrate de importar la entidad Team

@Module({
  imports: [TypeOrmModule.forFeature([Team])], // Configura TypeOrmModule para la entidad Team
  controllers: [TeamsController],
  providers: [TeamsService],
})
export class TeamsModule {}
