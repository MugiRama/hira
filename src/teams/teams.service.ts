import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { RoleDto } from './dto/rol.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Team } from './entities/team.entity';
import { User } from 'src/users/entities/user.entity'; // Asegúrate de ajustar la ruta según tu estructura
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

@Injectable()
export class TeamsService {
  constructor(
    @InjectRepository(Team)
    private readonly teamRepository: Repository<Team>,
  ) { }

  async create(createTeamDto: CreateTeamDto) {
    const newTeam = this.teamRepository.create(createTeamDto);
    const createdTeam = await this.teamRepository.save(newTeam);
    return createdTeam;
  }

  async findAll() {
    return await this.teamRepository.find();
  }

  async findOne(id: number) {
    return await this.teamRepository.findOneBy({ id });
  }

  async update(id: number, updateTeamDto: UpdateTeamDto) {
    await this.teamRepository.update(id, updateTeamDto);
    return `El team con el ID ${id} ha sido modificado`;
  }

  async remove(id: number) {
    await this.teamRepository.delete(id);
    return `El team con el ID ${id} ha sido removido`;
  }

  async addMemberToTeam(teamId: number, userId: number, roleId: number) {
    try {
      const team = await this.teamRepository
        .createQueryBuilder('team')
        .leftJoinAndSelect('team.members', 'members')
        .leftJoinAndSelect('members.roles', 'roles')
        .where('team.id = :teamId', { teamId })
        .getOneOrFail() as Team;
  
      // Verifica si el equipo tiene miembros
      if (!team.members) {
        throw new NotFoundException(`Equipo con ID ${teamId} no tiene miembros`);
      }
  
      // Verifica si el usuario ya es miembro del equipo
      const isMember = team.members.some((member) => member.id === userId);
      if (isMember) {
        throw new BadRequestException(`El usuario con ID ${userId} ya es miembro del equipo`);
      }
  
      // Busca el rol proporcionando opciones
      const member = team.members[0];
      if (!member) {
        throw new NotFoundException(`El equipo con ID ${teamId} no tiene miembros`);
      }
  
      if (!member.roles) {
        throw new NotFoundException(`El miembro del equipo con ID ${member.id} no tiene roles asignados`);
      }
  
      const role = member.roles.find((r) => r === roleId.toString()); // Convertir roleId a string
      if (!role) {
        throw new NotFoundException(`Rol con ID ${roleId} no encontrado`);
      }
  
      // Agrega el usuario al equipo y asigna el rol
      const user = await this.teamRepository.findOne({
        where: { id: userId },
        relations: ["roles"],
      });
      
  
      if (!user) {
        throw new NotFoundException(`Usuario con ID ${userId} no encontrado`);
      }
  
      if (!user.roles) {
        user.roles = [role];
      } else {
        user.roles.push(role);
      }
  
      team.members.push(user as unknown as User);
      await this.teamRepository.save(team);
  
      return `Usuario con ID ${userId} agregado al equipo con ID ${teamId} con rol asignado`;
    } catch (error) {
      if (error instanceof EntityNotFoundError) {
        throw new NotFoundException(`Equipo con ID ${teamId} no encontrado`);
      } else {
        throw error; // Puedes manejar otros tipos de errores aquí según sea necesario
      }
    }
  }
  

  async assignRoleToMember(_teamId: number, _userId: number, _roleId: number) {
    // Lógica para asignar un rol a un usuario en un equipo
  }

  async createRole(_roleData: RoleDto) {
    // Lógica para crear un nuevo rol
  }

  async updateRole(_roleId: number, _roleData: RoleDto) {
    // Lógica para actualizar un rol por ID
  }
}
