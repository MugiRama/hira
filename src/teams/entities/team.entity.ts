import { User } from "src/users/entities/user.entity";
import { Column, DeleteDateColumn, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Team {
  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column({ type: 'varchar', length: 255 }) 
  name!: string;

  @Column({ type: 'varchar', length: 255 }) // Utiliza el tipo de dato y la longitud adecuados
  description!: string;

  @ManyToMany(() => User, user => user.teams)
  @JoinTable()
  members: User[] | undefined;

  @DeleteDateColumn()
  deletedAt: Date | undefined;
  roles: any;
}
