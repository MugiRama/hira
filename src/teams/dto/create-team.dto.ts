import { IsString } from "class-validator";
import { User } from "src/users/entities/user.entity";

export class CreateTeamDto {
    id!: number;
    @IsString()
    name?: string;
    @IsString()
    description!: string;
    members!: User[];
}
  