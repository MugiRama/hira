import { PartialType } from '@nestjs/mapped-types';
import { CreateTeamDto } from './create-team.dto';
import { User } from 'src/users/entities/user.entity';
import { IsOptional, IsString, MinLength } from 'class-validator';

export class UpdateTeamDto extends PartialType(CreateTeamDto) {
    @IsOptional()
    id!: number;

    @IsString()
    @MinLength(1)
    @IsOptional()
    name?: string;

    @IsOptional()
    description!: string;
    
    @IsOptional()
    members!: User[];
}
