import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { TeamsService } from './teams.service';
import { CreateTeamDto } from './dto/create-team.dto';
import { UpdateTeamDto } from './dto/update-team.dto';
import { RoleDto } from './dto/rol.dto';

@Controller('teams')
export class TeamsController {
  constructor(private readonly teamsService: TeamsService) {}
  
  @Post()
  create(@Body() createTeamDto: CreateTeamDto) {
    return this.teamsService.create(createTeamDto);
  }

  @Get()
  findAll() {
    return this.teamsService.findAll();
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTeamDto: UpdateTeamDto) {
    return this.teamsService.update(+id, updateTeamDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.teamsService.remove(+id);
  }

  // Ruta para añadir un miembro a un equipo
  @Post(':teamId/members/:userId/roles/:roleId')
  addMemberToTeam(
    @Param('teamId') teamId: string,
    @Param('userId') userId: string,
    @Param('roleId') roleId: string
  ) {
    return this.teamsService.addMemberToTeam(+teamId, +userId, +roleId);
  }

  // Ruta para asignar un rol a un miembro en un equipo
  @Post(':teamId/members/:userId/roles/:roleId')
  assignRoleToMember(@Param('teamId') teamId: string, @Param('userId') userId: string, @Param('roleId') roleId: string) {
    return this.teamsService.assignRoleToMember(+teamId, +userId, +roleId);
  }

  // Rutas para crear/editar roles
  @Post('roles')
  createRole(@Body() roleData: RoleDto) {
    return this.teamsService.createRole(roleData);
  }

  @Patch('roles/:roleId')
  updateRole(@Param('roleId') roleId: string, @Body() roleData: RoleDto) {
    return this.teamsService.updateRole(+roleId, roleData);
  }
}
